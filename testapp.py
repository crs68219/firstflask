from flask import Flask, request, jsonify, g, render_template
#import sqlite3 as sql
import sqlite3
import pandas as pd

app = Flask(__name__)

# # if __name__ == '__main__':
# #     main()

def connect_db():
    return sqlite3.connect('blankdb.db')

def main():
    init_db()
    print("Main is running")
    app.run(debug=True)

def init_db():
    conn = connect_db()
    c = conn.cursor()
    c.execute("""DROP TABLE IF EXISTS dogs""")
    c.execute("""CREATE TABLE dogs(num TEXT, breed TEXT, temp TEXT, PRIMARY KEY('breed'));""") 
    conn.commit
    c.execute("""INSERT INTO dogs VALUES (2, 'Labrador', 'GoodBoy');""")
    c.execute("""INSERT INTO dogs VALUES (4, 'Pitbull', 'GoodBoy');""")
    c.execute("""INSERT INTO dogs VALUES (3, 'Boxer', 'GoodBoy');""")
    c.execute("""INSERT INTO dogs VALUES (2, 'Bichon', 'GoodBoy');""")
    c.execute("""INSERT INTO dogs VALUES (5, 'Bulldog', 'GoodBoy');""")
    c.execute("""INSERT INTO dogs VALUES (1, 'Shepard', 'GoodBoy');""")
    c.execute("""INSERT INTO dogs VALUES (0, 'None', 'BadDog');""")
    c.execute("""INSERT INTO dogs(num,breed,temp) VALUES(?,?,?)""",('100','Dalmatians','Goodboy'))
    c.execute("""INSERT INTO dogs(num,breed,temp) VALUES(?,?,?)""",('5','Cats','Selfish'))
    conn.commit()   #deleting these doesn't help, inserting the data a different way doesnt help conn vs c. doesnt help
    conn.close()
    
main()

@app.route("/")
def fun():
    name = request.args.get("name","World")
    place = request.args.get("place","Place")
    return jsonify({"response":"hello there " + name + " From: " + place})


# Url variables work
@app.route('/variable/<var>/<var2>')
def get_var(var = None,var2 = None):
    return("The variable is " + var + " " + var2)
        

@app.route("/test")
def fun2():
    return ("this is a test")


# # @application.before_request
# # def before_request():
# #     g.db = connect_db()

# THIS WORKS
@app.route("/add_maize")   
def add_rows():
    conn = connect_db()
    c = conn.cursor()
    c.execute("""DROP TABLE IF EXISTS maize""")
    c.execute("""CREATE TABLE maize(xcode TEXT, genotype TEXT, 
                   PRIMARY KEY('xcode') 
                   );""")
    c.execute("""INSERT INTO maize(xcode, genotype) VALUES(?,?)""",(1341, 'BloodyButcher'))
    c.execute("""INSERT INTO maize(xcode, genotype) VALUES(?,?)""",(1413, 'NavajoBlue'))
    c.execute("""INSERT INTO maize(xcode, genotype) VALUES(?,?)""",(5555, 'YellowDent'))
    cur = c.execute("""SELECT * from maize""")
    maizelist = [dict(xcode=row[0], genotype=row[1]) for row in cur]
    data = jsonify(maizelist)
    return data

#This also works
@app.route("/add_Parks/<parkname>/<parkstate>")   
def add_parks(parkname=None,parkstate=None):
    conn = connect_db()
    c = conn.cursor()
    c.execute("""DROP TABLE IF EXISTS parks""")
    c.execute("""CREATE TABLE parks(name TEXT, state TEXT, PRIMARY KEY('name') );""")
    c.execute("""INSERT INTO parks(name, state) VALUES(?,?)""",('BadLands', 'SD'))
    c.execute("""INSERT INTO parks(name, state) VALUES(?,?)""",('Yellowstone', 'WY'))
    c.execute("""INSERT INTO parks(name, state) VALUES(?,?)""",('GrandCanyon', 'AZ'))
    c.execute("""INSERT INTO parks(name, state) VALUES(?,?)""",(parkname, parkstate))
    conn.commit
    cur = c.execute("""SELECT * from parks""")
    parklist = [dict(name=row[0], state=row[1]) for row in cur]
    data = jsonify(parklist)
    return data



@app.route("/Dogs/<name>/")
def test(name=None):
    conn = connect_db()
    c = conn.cursor()
    cur = c.execute("""SELECT * FROM dogs where breed=?""", (name,))
    doglist = [dict(num=row[0], breed=row[1],temp=row[2]) for row in cur]
    data = jsonify(doglist)
    return data

@app.route("/get_dogs")
def get_dogs():
    conn = connect_db()
    c = conn.cursor()
    cur = c.execute("""SELECT breed FROM dogs""")
    dogslist = [dict(breed=row[0]) for row in cur]
    data = jsonify(dogslist)
    return data



@app.route("/shopping_list/<stuff>")
def add_shopping(stuff=None):
    conn = connect_db()
    c = conn.cursor()
    c.execute("""CREATE TABLE IF NOT EXISTS groceries(item TEST, PRIMARY KEY('item'));""")
    c.execute("""INSERT INTO groceries(item) VALUES(?)""",('milk',))
    c.execute("""INSERT INTO groceries(item) VALUES(?)""",('eggs',))
    c.execute("""INSERT INTO groceries(item) VALUES(?)""",('bread',))
    c.execute("""INSERT INTO groceries(item) VALUES(?)""",(stuff,))
    conn.commit
    cur = c.execute("""SELECT * from groceries""")
    grocerylist = [dict(items=row[0]) for row in cur]
    data = jsonify(grocerylist)
    return data








    # # helper to close
# @app.teardown_appcontext
# def close_connection(exception):
#     db = getattr(g, '_database', None)
#     if db is not None:
#         db.close()
    
# # helper method to get the database since calls are per thread,
# # and everything function is a new thread when called
# def get_db():
#     db = getattr(g, '_database', None)
#     if db is None:
#         db = g._database = sqlite3.connect(DATABASE)
#     return db


# # helper to close
# @app.teardown_appcontext
# def close_connection(exception):
#     db = getattr(g, '_database', None)
#     if db is not None:
#         db.close()    
    
    
    

# def init_db():
#     conn = connect_db()
#     c = conn.cursor()
#     try:
#         c.execute("""DROP TABLE IF EXISTS dogs""")
#         c.execute("""CREATE TABLE dogs(num TEXT, breed TEXT, temp TEXT 
#                        PRIMARY KEY('num') 
#                        );""")
#         c.execute("""INSERT INTO dogs(num, breed,temp) VALUES(?,?,?)""",(2, 'Labrador', 'GoodBoy'))
#         c.execute("""INSERT INTO dogs(num, breed,temp) VALUES(?,?,?)""",(4, 'Pitbull', 'GoodBoy'))
#         c.execute("""INSERT INTO dogs(num, breed,temp) VALUES(?,?,?)""",(3, 'Boxer', 'GoodBoy'))
#         c.execute("""INSERT INTO dogs(num, breed,temp) VALUES(?,?,?)""",(2, 'Bichon', 'GoodBoy'))
#         c.execute("""INSERT INTO dogs(num, breed,temp) VALUES(?,?,?)""",(5, 'Bulldog', 'GoodBoy'))
#         c.execute("""INSERT INTO dogs(num, breed,temp) VALUES(?,?,?)""",(1, 'Shepard', 'GoodBoy'))
#         c.execute("""INSERT INTO dogs(num, breed,temp) VALUES(?,?,?)""",(0, 'None', 'BadDog'))
#         c.commit()
        
#     finally:
#         c.close()



















#have to make an evironment variable to run from the terminal

# DATABASE = 'flaskapp2/assignment2.db'



# #I THINK THIS WORKS
# @app.route("/add_maize")   
# def add_rows():
#     conn = connect_db()
#     c = conn.cursor()
#     c.execute("""DROP TABLE IF EXISTS maize""")
#     c.execute("""CREATE TABLE maize(xcode TEXT, genotype TEXT, 
#                    PRIMARY KEY('xcode') 
#                    );""")
#     c.execute("""INSERT INTO maize(xcode, genotype) VALUES(?,?)""",(1341, 'TEST'))
#     c.execute("""INSERT INTO maize(xcode, genotype) VALUES(?,?)""",(1413, 'TESTb'))
#     cur = c.execute("""SELECT * from maize""")
#     maizelist = [dict(xcode=row[0], genotype=row[1]) for row in cur]
#     data = jsonify(maizelist)
#     return data


# @app.route("/add_Dogs")   
# def add_dogs():
#     conn = connect_db()
#     c = conn.cursor()
#     c.execute("""DROP TABLE IF EXISTS dogs""")
#     c.execute("""CREATE TABLE dogs(num TEXT, breed TEXT, 
#                    PRIMARY KEY('num') 
#                    );""")
#     c.execute("""INSERT INTO dogs(num, breed) VALUES(?,?)""",(2, 'Labrador'))
#     c.execute("""INSERT INTO dogs(num, breed) VALUES(?,?)""",(4, 'Pitbull'))
#     cur = c.execute("""SELECT * from dogs""")
#     puplist = [dict(dog=row[0], breed=row[1]) for row in cur]
#     data = jsonify(puplist)
#     return data
        
# @app.route("/list")
# def fun3():
#     conn = connect_db()
#     c = conn.cursor()
#     cur = c.execute("""SELECT * from maize""")
#     maizelist = [dict(xcode=row[0], genotype=row[1]) for row in cur]
#     data = jsonify(cur)
#     return data
#     #return("This is where maize list should go")
    
# def add_maize(xcode='1369',genotype = 'TEST'):
#     sql = "INSERT INTO maize(xcode,genotype) VALUES('%d','%s')"%(int(xcode),genotype)
#     print(sql)
#     db = get_db()
#     db.execute(sql)
#     res = db.commit()
#     return res
 

# # @app.route("/add", methods = ['POST'])
# # def add_user():
# #     print(add_maize(xcode=request.form['xcode'], genotype=request.form['genotype']))
# #     return ''

# def connect_db():
#     return sqlite3.connect('blankdb.db')

# def connect_db():
#     return sqlite3.connect('flaskapp2/assignment2.db')

# def get_db():
#     db = getattr(g, '_database', None)
#     if db is None:
#         db = g._database = sqlite3.connect(DATABASE)
#         db.row_factory = sqlite3.Row
#     return db

# @app.route('/soildata')
# def city_list():
#     db = get_db()
#     data = db.execute('SELECT * FROM soil').fetchall()
#     return (data)





