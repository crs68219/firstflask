I was having trouble getting my original db to connect, so I started from scratch with some simple queries and tables. 

URL = _ 

Commands and Descriptions:

URL/get_dogs
This gives you a list of dogs that are availabe in the dogs table. Use this to pick a breed for the next query.

URL/Dogs/<breed>/
This url takes any breed as a URL parameter. Pick a breed from the list given by get_dogs. 
It will give you some information on the breed such as number, breed, and temperment(are they a good boy?)

URL/add_Parks/<parkname>/<parkstate>
I have given you a table of three National Parks you should visit. 
To add your own to the list, input the name of the park and the state it is in, and you will see it added to my list!

